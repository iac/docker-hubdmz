FROM alpine AS dhparams

RUN apk add --no-cache openssl && \
    openssl dhparam -out /etc/ssl/dh4096.pem 4096


FROM nginx:stable-alpine

RUN apk add --no-cache nftables nftables-openrc

COPY --from=dhparams /etc/ssl/dh4096.pem /etc/nginx/dh4096.pem
ADD firewall.sh /docker-entrypoint.d/05-firewall.sh
ADD nftables.conf /etc/conf.d/nftables
ADD nftables.nft /etc/nftables.nft
ADD nginx.conf /etc/nginx/nginx.conf
